package World;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author billy
 */
public class GameProtocol {
    public static final String DELIMITER = "::";
    public static final String WORLD_PROPERTY = "WORLD_PROPERTY";
    public static final String WORLD_PROPERTY_REQUEST = "WORLD_PROPERTY_REQUEST";
    public static final String WARRIOR_PROPERTY = "WARRIOR_PROPERTY";
    public static final String WARRIOR_PROPERTY_REQUEST = "WARRIOR_PROPERTY_REQUEST";
    public static final String PRODUCE_WARRIOR = "PRODUCE_WARRIOR";
    public static final String PRODUCE_WARRIOR_REQUEST = "PRODUCE_WARRIOR_REQUEST";
}
