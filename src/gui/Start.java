/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *
 * @author billy
 */
public class Start implements Initializable {
    
    @FXML
    private void handleButtonDisplayAssignment1(ActionEvent event) {
        Main.getInstance().displayAssignment1();
    }
    
    @FXML
    private void handleButtonSinglePlayer(ActionEvent event) {
        Main.getInstance().startSinglePlayerMode();
    }
    
    @FXML
    private void handleButtonMultiPlayer(ActionEvent event) {
        Main.getInstance().startMultiPlayerMode();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
