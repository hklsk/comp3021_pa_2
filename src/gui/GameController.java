/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Warriors.Warrior;
import Warriors.WarriorType;
import World.City;
import World.GameProtocol;
import World.World;
import World.WorldEventListener;
import World.WorldProperty;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
/**
 * FXML Controller class
 *
 * @author billy
 */
public class GameController implements Initializable {

    public static final int SERVER_MODE = 1;
    public static final int CLIENT_MODE = 2;
    public static final int GAME_PORT = 8888;
    public static final int GAME_TIME_OUT = 5000;
    @FXML
    private AnchorPane anchorPane_city;
    @FXML
    private AnchorPane anchorPane_control;
    @FXML
    private ImageView imageView_warrior;
    @FXML
    private Label label_clock;
    @FXML
    private Label label_lifeElement;
    @FXML
    private Label label_error;
    @FXML
    private Label label_type;
    @FXML
    private Label label_party;
    @FXML
    private Label label_id;
    @FXML
    private Label label_location;
    @FXML
    private Label label_hp;
    @FXML
    private Label label_attack;
    @FXML
    private Label label_kill;
    @FXML
    private Label label_steps;
    private CityGridPane[] cityGridPanes;
    private static GameController instance;
    private City city;
    private Warrior warrior;
    private Runnable worldRunnable;
    private Thread worldThread;
    private volatile CountDownLatch inputLatch;
    private volatile World world;
    private Thread countDownThread;
    private Thread serverThread;
    private Thread clientThread;
    private ServerSocket serverSocket;
    private boolean canProduce;
    private int multiplayerMode;
    private String serverAddress;
    private int party = -1;
    private volatile Socket socket;
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
    }
    
    public GameController init() {
        label_error.setText("");
        this.party = WorldProperty.RED;
        if(World.GameMode == World.MULTI_PLAYER_GAME_MODE) {
            if(this.multiplayerMode == SERVER_MODE) {
                label_error.setText("Waiting for client...");
                serverThread = new Thread(() -> {
                    try {
                        serverSocket = new ServerSocket(GAME_PORT);
                        socket = serverSocket.accept(); //only accept 1 client
                        Platform.runLater(() -> {
                            label_error.setText("");
                        });
                        BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter os = new PrintWriter(socket.getOutputStream(), true);
                        String response;
                        while ((response = is.readLine()) != null) {
//                            System.out.println("Server: " + response);
                            String [] pieces = response.split(GameProtocol.DELIMITER, 2);
                            String event = pieces[0];
                            String message = "";
                            if(pieces.length > 1) {
                                message = pieces[1];
                            }
                            if(event.equals(GameProtocol.WORLD_PROPERTY_REQUEST)) {
                                os.printf("%s%s%d,%d,%d", GameProtocol.WORLD_PROPERTY, GameProtocol.DELIMITER, WorldProperty.InitLifeElements, WorldProperty.NumberOfCity, WorldProperty.MaxMinutes);
                                os.println();
                            }
                            if(event.equals(GameProtocol.WARRIOR_PROPERTY_REQUEST)) {
                                os.printf("%s%s", GameProtocol.WARRIOR_PROPERTY, GameProtocol.DELIMITER);
                                for(int i=0; i<WarriorType.HP_LIST.length; i++) {
                                    os.printf("%d,%d,%d,", i, WarriorType.HP_LIST[i], WarriorType.ATTACK_LIST[i]);
                                }
                                os.println();
                                this.start();
                            }
                            if(event.equals(GameProtocol.PRODUCE_WARRIOR)) {
                                world.produce(Integer.parseInt(message), WorldProperty.BLUE);
                                inputLatch.countDown();
                            }
                        }
                    } catch (IOException e) {
                        Platform.runLater(() -> {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setHeaderText("Error");
                            alert.setContentText(e.getMessage());
                            alert.showAndWait();
                        });
                        Logger.getLogger(MultiPlayerController.class.getName()).log(Level.SEVERE, null, e);
                    }
                });
                serverThread.start();
            } else {
                label_error.setText("Connecting...");
                this.party = WorldProperty.BLUE;
                clientThread = new Thread(() -> {
                    try {
                        socket = new Socket();
                        socket.connect(new InetSocketAddress(serverAddress, GAME_PORT), GAME_TIME_OUT);
                        Platform.runLater(() -> {
                            label_error.setText("");
                        });
                        BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter os = new PrintWriter(socket.getOutputStream(), true);
                        String response;
                        os.println(GameProtocol.WORLD_PROPERTY_REQUEST);
                        os.println(GameProtocol.WARRIOR_PROPERTY_REQUEST);
                        while ((response = is.readLine()) != null) {
//                            System.out.println("Client: " + response);
                            String [] pieces = response.split(GameProtocol.DELIMITER, 2);
                            String event = pieces[0];
                            String message = "";
                            if(pieces.length > 1) {
                                message = pieces[1];
                            }
                            if(event.equals(GameProtocol.WORLD_PROPERTY)) {
                                String[] tmp = message.split(",");
                                WorldProperty.InitLifeElements = Integer.parseInt(tmp[0]);
                                WorldProperty.NumberOfCity = Integer.parseInt(tmp[1]);
                                WorldProperty.MaxMinutes = Integer.parseInt(tmp[2]);
                                Platform.runLater(() -> {
                                    setUpLayout();
                                });
                            }
                            if(event.equals(GameProtocol.WARRIOR_PROPERTY)) {
                                String[] tmp = message.split(",");
                                for(int i=0; i<tmp.length; i+=3) {
                                    int index = Integer.parseInt(tmp[i]);
                                    WarriorType.HP_LIST[index] = Integer.parseInt(tmp[i+1]);
                                    WarriorType.ATTACK_LIST[index] = Integer.parseInt(tmp[i+2]);
                                }
                                this.start();
                            }
                            if(event.equals(GameProtocol.PRODUCE_WARRIOR)) {
                                world.produce(Integer.parseInt(message), WorldProperty.RED);
                                inputLatch.countDown();
                            }
                        }
                    } catch (IOException | NumberFormatException e) {
                        Platform.runLater(() -> {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setHeaderText("Error");
                            alert.setContentText(e.getMessage());
                            alert.showAndWait();
                        });
                        Logger.getLogger(MultiPlayerController.class.getName()).log(Level.SEVERE, null, e);
                    }
                });
                clientThread.start();
            }
        }
        Platform.runLater(() -> {
            setUpLayout();
        });
        if(World.GameMode != World.MULTI_PLAYER_GAME_MODE) {
            this.start();
        }
        return this;
    }
    
    private synchronized void setUpLayout() {
        cityGridPanes = new CityGridPane[WorldProperty.NumberOfCity+2];
        anchorPane_city.getChildren().clear();
        for(int i=0; i<WorldProperty.NumberOfCity+2; i++) {
            cityGridPanes[i] = new CityGridPane().init();
            CityGridPane cityGridPane = cityGridPanes[i];
            if(i == 0 || i == WorldProperty.NumberOfCity+1) {
                cityGridPane.setType(CityGridPane.HEAD_QUARTER);
            } else {
                cityGridPane.setType(CityGridPane.CITY);
            }
            cityGridPane.setLayoutX(110 * i);
            cityGridPane.setLayoutY(10);
            cityGridPane.update();
            anchorPane_city.getChildren().add(cityGridPane);
        }
        WarriorGridPane warriorGridPane = new WarriorGridPane();
        warriorGridPane.init();
        warriorGridPane.setLayoutX(0);
        warriorGridPane.setLayoutY(0);
        anchorPane_control.getChildren().clear();
        anchorPane_control.getChildren().add(warriorGridPane);
    }
    
    public GameController setWarrior(Warrior warrior) {
        this.warrior = warrior;
        this.update();
        return this;
    }
    
    public GameController setCity(City city) {
        this.city = city;
        this.update();
        return this;
    }
    
    public void start() {
        worldRunnable = () -> {
            world = new World();
            world.setWorldEventListener(new WorldEventListener() {
                @Override
                public void run(int eventId, Object ...args) {
                    if(eventId == WorldEventListener.WAIT_FOR_USER_INPUT) {
                        inputLatch = (CountDownLatch) args[0];
                        canProduce = true;
                        countDownThread = new Thread(() -> {
                            try {
                                Thread.sleep(10000);
                                canProduce = false;
                                if(countDownThread == Thread.currentThread() && inputLatch != null) {
                                    inputLatch.countDown();
                                    if(World.GameMode == World.MULTI_PLAYER_GAME_MODE) {
                                        inputLatch.countDown();
                                    }
                                }
                            } catch (InterruptedException e) {
                            }
                        });
                        countDownThread.start();
                    }
                }
            });
            world.runGame();
        };
        worldThread = new Thread(worldRunnable);
        worldThread.start();
    }
    
    public GameController setMultiplayerMode(int multiplayerMode) {
        this.multiplayerMode = multiplayerMode;
        return this;
    }
    
    public GameController setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
        return this;
    }
    
    public void produceWarrior(int type) {
        if(canProduce) {
            canProduce = false;
            if(world.produce(type, this.party)) {
                if(World.GameMode != World.MULTI_PLAYER_GAME_MODE) {
                    countDownThread.interrupt();
                    countDownThread = null;
                }
                inputLatch.countDown();
                if(World.GameMode == World.MULTI_PLAYER_GAME_MODE) {
                    try {
                        PrintWriter os = new PrintWriter(socket.getOutputStream(), true);
                        os.printf("%s%s%d", GameProtocol.PRODUCE_WARRIOR, GameProtocol.DELIMITER, type);
                        os.println();
                    } catch (IOException ex) {
                        Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                label_error.setText("");
            } else {
                label_error.setText("Not enough life elements");
                canProduce = true;
            }
        } else {
            label_error.setText("You can not produce warrior now");
        }
    }
    
    public void update() {
        Platform.runLater(() -> {
            if (World.CityList != null) {
                for (int i = 0; i<World.CityList.size(); i++) {
                    City city = World.CityList.get(i);
                    cityGridPanes[i].setCity(city);
                    if (city.RedWarriorStation.size() > 0) {
                        cityGridPanes[i].setRedWarrior(city.RedWarriorStation.get(0));
                    } else {
                        cityGridPanes[i].setRedWarrior(null);
                    }
                    if (city.BlueWarriorStation.size() > 0) {
                        cityGridPanes[i].setBlueWarrior(city.BlueWarriorStation.get(0));
                    } else {
                        cityGridPanes[i].setBlueWarrior(null);
                    }
                    cityGridPanes[i].update();
                }
            }
            if(city != null) {            
                label_lifeElement.setText(""+city.LifeElement);
            }
            if(warrior != null) {
                imageView_warrior.setImage(new Image("/figure/"+WarriorType.WarriorNames[warrior.Type]+"_"+WorldProperty.PartyNames[warrior.Party]+".png"));
                label_type.setText(WarriorType.WarriorNames[warrior.Type]);
                label_party.setText(WorldProperty.PartyNames[warrior.Party]);
                label_id.setText(""+warrior.ProductionID);
                if(warrior.Location == 0) {
                    label_location.setText(WorldProperty.PartyNames[0]+" Headquarter");
                } else if(warrior.Location == World.CityList.size()-1) {
                    label_location.setText(WorldProperty.PartyNames[1]+" Headquarter");
                } else {
                    label_location.setText("City "+warrior.Location);
                }
                
                label_hp.setText(""+warrior.HP);
                label_attack.setText(""+warrior.AttackValue);
                label_kill.setText(""+warrior.NumberOfKilledWarrior);
                label_steps.setText(""+warrior.MovedDistance);
            }
            if(World.WorldClock != null) {
                label_clock.setText(World.WorldClock.getTime());
            }
        });
        
        //System.out.println("Update UI");
    }
    
    public static GameController getInstance() {
        return instance;
    }
}
