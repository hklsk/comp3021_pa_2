/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Warriors.WarriorType;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author billy
 */
public class InputWarriorController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ImageView imageView_icon;
    @FXML
    private Label label_title;
    @FXML
    private TextField textField_hp;
    @FXML
    private TextField textField_attack;
    private int warriorIndex;
    
    @FXML
    private void handleButtonSave(ActionEvent event) {
        try {
            WarriorType.HP_LIST[warriorIndex] = Integer.parseInt(textField_hp.getText());
            WarriorType.ATTACK_LIST[warriorIndex] = Integer.parseInt(textField_attack.getText());
            Main.getInstance().configWarrior(warriorIndex+1);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }
    }
    
    public void init(int index) {
        warriorIndex = index;
        imageView_icon.setImage(new Image("/figure/"+WarriorType.WarriorNames[warriorIndex]+".png"));
        label_title.setText(WarriorType.WarriorNames[warriorIndex]);
        textField_hp.setText("0");
        textField_attack.setText("0");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
