/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Warriors.WarriorType;
import World.World;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author billy
 */
public class Main extends Application {
    
    private Stage mainStage;
    private Map<String, Scene> scenes;
    private Map<String, Object> sceneControllers;
    public static final String[] FXML_RESOURCES = {"Start", "InputEnvironment", "InputWarrior", "Game", "MultiPlayer"};
    private static Main instance = null;
    
    public Main() {
        instance = this;
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        scenes = new HashMap<String, Scene>();
        sceneControllers = new HashMap<String, Object>(); 
        for(String resource : FXML_RESOURCES) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(resource + ".fxml"));
            Scene scene = new Scene(loader.load());
            scenes.put(resource, scene);
            sceneControllers.put(resource, loader.getController());
        }
        mainStage = stage;
        stage.setScene(scenes.get("Start"));
        stage.show();
    }
    
    public void displayAssignment1() {
        WarriorType.HP_LIST = new int[WarriorType.WarriorNames.length];
        WarriorType.ATTACK_LIST = new int[WarriorType.WarriorNames.length];
        mainStage.setScene(scenes.get("InputEnvironment"));
        World.GameMode = World.AUTO_GAME_MODE;
    }
    
    public void configWarrior() {
        configWarrior(0);
    }
    
    public void configWarrior(int index) {
        if(index < WarriorType.WarriorNames.length) {
            InputWarriorController inputWarriorController = (InputWarriorController) sceneControllers.get("InputWarrior");
            inputWarriorController.init(index);
            mainStage.setScene(scenes.get("InputWarrior"));
        } else {
            mainStage.setScene(scenes.get("Game"));
            ((GameController) sceneControllers.get("Game")).init();
        }
    }
    
    public void startSinglePlayerMode() {
        WarriorType.HP_LIST = new int[WarriorType.WarriorNames.length];
        WarriorType.ATTACK_LIST = new int[WarriorType.WarriorNames.length];
        mainStage.setScene(scenes.get("InputEnvironment"));
        World.GameMode = World.SINGLE_PLAYER_GAME_MODE;
    }
    
    public void startMultiPlayerMode() {
        mainStage.setScene(scenes.get("MultiPlayer"));
    }
    
    public void startMultiPlayerServerMode() {
        WarriorType.HP_LIST = new int[WarriorType.WarriorNames.length];
        WarriorType.ATTACK_LIST = new int[WarriorType.WarriorNames.length];
        mainStage.setScene(scenes.get("InputEnvironment"));
        World.GameMode = World.MULTI_PLAYER_GAME_MODE;
        ((GameController) sceneControllers.get("Game")).setMultiplayerMode(GameController.SERVER_MODE);
    }
    
    public void startMultiPlayerClientMode(String serverAddress) {
        World.GameMode = World.MULTI_PLAYER_GAME_MODE;
        mainStage.setScene(scenes.get("Game"));
        ((GameController) sceneControllers.get("Game")).setMultiplayerMode(GameController.CLIENT_MODE).setServerAddress(serverAddress).init();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static Main getInstance() {
        return instance;
    }
    
}
