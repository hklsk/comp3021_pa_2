/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import World.WorldProperty;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author billy
 */
public class InputEnvironmentController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private TextField textField_noOfLifeElement;
    @FXML
    private TextField textField_noOfCity;
    @FXML
    private TextField textField_maxMinute;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    private void handleButtonSave(ActionEvent event) {
        try {
            WorldProperty.InitLifeElements = Integer.parseInt(textField_noOfLifeElement.getText());
            WorldProperty.NumberOfCity = Integer.parseInt(textField_noOfCity.getText());
            WorldProperty.MaxMinutes = Integer.parseInt(textField_maxMinute.getText());
            Main.getInstance().configWarrior();
        } catch(Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }
    }
    
}
