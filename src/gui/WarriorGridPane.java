package gui;

import Warriors.WarriorType;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author billy
 */
public class WarriorGridPane extends GridPane {
    public static final int WIDTH = 120;
    public static final int HEIGHT = 120;
    public static final int COL_SIZE = 3;
    private ImageView[] imageView_warriors;
    
    public WarriorGridPane init() {
        this.setPrefSize(WIDTH, HEIGHT);
        this.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        imageView_warriors = new ImageView[WarriorType.WarriorNames.length];
        for(int i=0; i<WarriorType.WarriorNames.length; i++) {
            imageView_warriors[i] = new ImageView();
            imageView_warriors[i].setFitWidth(WIDTH*1.0f/COL_SIZE);
            imageView_warriors[i].setFitHeight(HEIGHT/Math.ceil(WarriorType.WarriorNames.length*1.0f/COL_SIZE)*1.0f);
            imageView_warriors[i].setPreserveRatio(false);
            imageView_warriors[i].setUserData(new Integer(i));
            imageView_warriors[i].setOnMousePressed(new EventHandler() {
                @Override
                public void handle(Event event) {
                    ImageView imageView = (ImageView) event.getTarget();
                    GameController.getInstance().produceWarrior(((Integer) imageView.getUserData()).intValue());
                }
            });
            imageView_warriors[i].setImage(new Image("figure/"+WarriorType.WarriorNames[i]+".png"));
            this.add(imageView_warriors[i], i % COL_SIZE, (int) Math.ceil(i/COL_SIZE));
        }
        return this;
    }
    
}
