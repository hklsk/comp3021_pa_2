/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Warriors.Warrior;
import Warriors.WarriorType;
import World.City;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

/**
 *
 * @author billy
 */
public class CityGridPane extends GridPane {
    public static final int WIDTH = 100;
    public static final int HEIGHT = 200;
    public static final int CITY = 0;
    public static final int HEAD_QUARTER = 1;
    public static final int FLAG_RED = 0;
    public static final int FLAG_BLUE = 1;
    public static final String[] FLAGS = {"flag_red", "flag_blue"};
    private ImageView imageView_flag;
    private ImageView imageView_city;
    private ImageView imageView_redWarrior;
    private ImageView imageView_blueWarrior;
    private int type = -1;
    private int flagType = -1;
    private City city;
    private Warrior redWarrior;
    private Warrior blueWarrior;
    private CityGridPane self;
    public CityGridPane() {
        super();
        self = this;
    }
    public CityGridPane init() {
        this.setPrefSize(WIDTH, HEIGHT);
        this.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        imageView_flag = new ImageView();
        imageView_city = new ImageView();
        imageView_redWarrior = new ImageView();
        imageView_blueWarrior = new ImageView();
        imageView_flag.setFitWidth(WIDTH*.5);
        imageView_flag.setFitHeight(HEIGHT*.25);
        imageView_flag.setPreserveRatio(false);
        GridPane.setMargin(imageView_flag, new Insets(0, 0, 0, WIDTH*.25));
        imageView_city.setFitWidth(WIDTH);
        imageView_city.setFitHeight(HEIGHT*.5);
        imageView_city.setPreserveRatio(false);
        imageView_city.setOnMousePressed(new EventHandler() {
            @Override
            public void handle(Event event) {
                GameController.getInstance().setCity(city);
            }
        });
        imageView_redWarrior.setFitWidth(WIDTH*.5);
        imageView_redWarrior.setFitHeight(HEIGHT*.25);
        imageView_redWarrior.setPreserveRatio(false);
        imageView_blueWarrior.setFitWidth(WIDTH*.5);
        imageView_blueWarrior.setFitHeight(HEIGHT*.25);
        imageView_blueWarrior.setPreserveRatio(false);
        imageView_redWarrior.setOnMousePressed(new EventHandler() {
            @Override
            public void handle(Event event) {
                if(self.redWarrior != null) {
                    GameController.getInstance().setWarrior(self.redWarrior);
                }
            }
        });
        imageView_blueWarrior.setOnMousePressed(new EventHandler() {
            @Override
            public void handle(Event event) {
                if(self.blueWarrior != null) {
                    GameController.getInstance().setWarrior(self.blueWarrior);
                }
            }
        });
        this.add(imageView_flag, 0, 0, 2, 1);
        this.add(imageView_city, 0, 1, 2, 2);
        this.add(imageView_redWarrior, 0, 3, 1, 1);
        this.add(imageView_blueWarrior, 1, 3, 1, 1);
        return this;
    }
    public CityGridPane setType(int type) {
        this.type = type;
        return this;
    }
    public CityGridPane setCity(City city) {
        this.city = city;
        return this;
    }
    public CityGridPane setRedWarrior(Warrior warrior) {
        this.redWarrior = warrior;
        return this;
    }
    public CityGridPane setBlueWarrior(Warrior warrior) {
        this.blueWarrior = warrior;
        return this;
    }
    public void update() {
        imageView_city.setImage(new Image("figure/castle_"+this.type+".png"));
        if(this.city != null && this.city.Flag != -1) {
            imageView_flag.setImage(new Image("figure/"+FLAGS[this.city.Flag]+".png"));
        } else {
            imageView_flag.setImage(null);
        }
        if(redWarrior != null) {
            imageView_redWarrior.setImage(new Image("/figure/"+WarriorType.WarriorNames[redWarrior.Type]+"_red.png"));
        } else {
            imageView_redWarrior.setImage(null);
        }
        if(blueWarrior != null) {
            imageView_blueWarrior.setImage(new Image("/figure/"+WarriorType.WarriorNames[blueWarrior.Type]+"_blue.png"));
        } else {
            imageView_blueWarrior.setImage(null);
        }
    }
}
