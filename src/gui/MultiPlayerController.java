/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import World.WorldProperty;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author billy
 */
public class MultiPlayerController implements Initializable {

    
    @FXML
    private Label label_hostIP;
    @FXML
    private TextField textField_clientIP;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            InetAddress addr = InetAddress.getLocalHost();
            label_hostIP.setText(addr.getHostAddress());
        } catch (UnknownHostException e) {
            label_hostIP.setText(e.getMessage());
        }
    }
    
    @FXML
    private void handleButtonCreateGame(ActionEvent event) {
        Main.getInstance().startMultiPlayerServerMode();
    }
    
    @FXML
    private void handleButtonJoinGame(ActionEvent event) {
        Main.getInstance().startMultiPlayerClientMode(textField_clientIP.getText());
    }
}
